// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyShooter/FuncLibrary/LibTypes.h"
#include "TimerManager.h"
#include "MyShooterCharacter.generated.h"

UCLASS(Blueprintable)
class AMyShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMyShooterCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* InputMoveComponent) override;



	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

public:

	//Movement 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	EMovementState MovementState = EMovementState::Normal_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	FCharacterSpeed MovementInfo;

	//movement mod
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool RunEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool WalkEnable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	bool AimEnable = false;


	UFUNCTION()
	void MoveInputAxisY(float Value);
	UFUNCTION()
	void MoveInputAxisX(float Value);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
	void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
	void ChangeMovementState();

	UFUNCTION(BlueprintCallable)
	void RunMovementState();

	FVector LookDirection;
	FVector MoveDirection;
	float SpeedModif = 1.0f;

	//Boom camera wheelth
	UFUNCTION(BlueprintCallable)
	void BCameraInputAxis(float Value);

	float CameraAxis = 0.0f;

	UFUNCTION(BlueprintCallable)
	void BoomCameraWeight();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraWeightChange = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraMaxWeight = 3000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float CameraMinWeight = 200.0f;

	bool bIsBloom;

};

