// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "TimerManager.h"
#include "Math.h"
#include "Engine/World.h"
#include "Engine/World.h"

AMyShooterCharacter::AMyShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	bIsBloom = false;
	//CameraArmLength = 800.0f;
	//CameraBloomSpeed = 5.0f;
}

void AMyShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);
}

void AMyShooterCharacter::SetupPlayerInputComponent(UInputComponent* InputMoveComponent)
{
	Super::SetupPlayerInputComponent(InputMoveComponent);

	InputMoveComponent->BindAxis(TEXT("MoveForward"), this, &AMyShooterCharacter::MoveInputAxisX);
	InputMoveComponent->BindAxis(TEXT("MoveRight"), this, &AMyShooterCharacter::MoveInputAxisY);
}

void AMyShooterCharacter::MoveInputAxisY(float Value)
{
	AxisY = Value;
}

void AMyShooterCharacter::MoveInputAxisX(float Value)
{
	AxisX = Value;
}

void AMyShooterCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

	FHitResult ResultHit;

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery3, false, ResultHit);

		SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw, 0.0f)));
	}
}

void AMyShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 350;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
/*		LookDirection = GetActorForwardVector();
		MoveDirection = FVector(AxisX, AxisY, 0.0f).GetSafeNormal();

		if (!MoveDirection.IsNearlyZero())
		{
			float DotProduct = FVector::DotProduct(MoveDirection, LookDirection);
			float Angle = FMath::Acos(DotProduct) * 180 / PI;


			SpeedModif = FMath::Clamp(1.0f - (Angle / 180), 0.0f, 1.0f);
		}
		ResSpeed *= SpeedModif;
		
		if (ResSpeed < MovementInfo.NormalSpeed)
		{
			ResSpeed = MovementInfo.NormalSpeed;
		}*/
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Normal_State:
		ResSpeed = MovementInfo.NormalSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AMyShooterCharacter::ChangeMovementState()
{
	if (!WalkEnable && !RunEnable && !AimEnable)
	{
		MovementState = EMovementState::Normal_State;
	}
	else 
	{
		if (RunEnable)
		{
			WalkEnable = false;
			AimEnable = false;
			MovementState = EMovementState::Run_State;
		}
		if (WalkEnable && !RunEnable && AimEnable)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnable && !RunEnable && !AimEnable)
			{
				MovementState = EMovementState::Walk_State;
			}
			else if(!WalkEnable && !RunEnable && AimEnable)
			{
				MovementState = EMovementState::Aim_State;
			}
		}
	}
	CharacterUpdate();
}

void AMyShooterCharacter::RunMovementState()
{
	float ResSpeed = MovementInfo.RunSpeed;
	LookDirection = GetActorForwardVector();
	MoveDirection = FVector(AxisX, AxisY, 0.0f).GetSafeNormal();

	if (!MoveDirection.IsNearlyZero())
	{
		float DotProduct = FVector::DotProduct(MoveDirection, LookDirection);
		float Angle = FMath::Acos(DotProduct) * 180 / PI;


		SpeedModif = FMath::Clamp(1.0f - (Angle / 180), 0.0f, 1.0f);
	}
	ResSpeed *= SpeedModif;
	if (ResSpeed < MovementInfo.NormalSpeed)
	{
		ResSpeed = MovementInfo.NormalSpeed;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AMyShooterCharacter::BCameraInputAxis(float Value)
{
	CameraAxis = Value;
	BoomCameraWeight();
}

void AMyShooterCharacter::BoomCameraWeight()
{
	float CurrentCameraWeight = CameraBoom->TargetArmLength;

	if (!bIsBloom)
	{
		if (CameraAxis < 0)
		{
			if ((CurrentCameraWeight + CameraWeightChange) < CameraMaxWeight)
			{
				CameraBoom->TargetArmLength = CurrentCameraWeight + CameraWeightChange;
			}
		}
		else if (CameraAxis > 0)
		{
			if ((CurrentCameraWeight - CameraWeightChange) > CameraMinWeight)
			{	
				CameraBoom->TargetArmLength = CurrentCameraWeight - CameraWeightChange;
			}
		}
	}

}
